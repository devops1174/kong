# Kong Api Gateway

```
git clone https://gitlab.com/devops1174/kong.git

cd kong
cd kong-server
docker-compose up -d
docker-compose ps

cd ..
cd app-server
docker-compose ps
docker-compose start
```

## solve elk start fail

```
wsl --list
wsl -d docker-desktop
sysctl -w vm.max_map_count=262144
exit
docker compose up -d elk
```

## Start Kong

```
cd kong-server
docker-compose up -d
```

## Konga Web Admin

```
oper browser : http://{your-server-ip}:1337/
```

## java spring boot

```
https://github.com/Sommaik/demo-service-01
```

```
docker build -t spring .
docker tag spring registry.gitlab.com/devops1174/kong/spring:2.0
docker push registry.gitlab.com/devops1174/kong/spring:2.0
```

## stop all service

```
docker-compose stop

docker-compose rm elk

docker-compose up -d elk
```

## Workshop

```
kong : http://localhost:8000/spring
service : http://localhost:8181
plugins:
 - proxy-cache
 - ldap-auth

# test via kong:
http://localhost:8000/spring/users
http://localhost:8000/spring/todos

# test direct
http://localhost:8181/users
http://localhost:8181/todos
```

```
cd app-server
docker-compose up -d db
docker-compose up -d mogodb
docker-compose up -d flyway
docker-compose up -d apisvr

http://localhost:3030/news
```

```
docker-compose down
git pull
cd kong-server
docker-compose up -d

```

```
https://github.com/Sommaik/devops

https://gitlab.com/sommai.k

docker compose -p kong-server stop kong
docker compose -p kong-server rm kong
docker compose -p kong-server up -d kong
```

```
"provision_key": "IDDZiI9kadarONU6daWKE46M9CsL4vf0"

"client_id": "ym3XPdA2Br0G9kt91D430d3Rq23zcVnp",
"client_secret": "yvO6Et73DwFOyjywKXoJJTL0EnPar2vB",

"code": "e2KP6Y85ZQx8fBGt0sPwvk53vOfSOpDI"
```

## K8s deployment step

```
cd k8s
kubectl apply -f kong-config.yml
kubectl apply -f kong-pg-data.yml
kubectl apply -f kong-pg.yml
kubectl apply -f kong-job.yml
kubectl apply -f kong-gw.yml
kubectl apply -f konga.yml
kubectl port-forward service/konga 1337:1337
```

```
kubectl apply -f api-mongo.yml
kubectl apply -f api-mysql.yml
kubectl apply -f api-svr.yml
kubectl apply -f spring.yml
kubectl port-forward service/kong 8000:8000

kubectl apply -f zipkin.yml
kubectl port-forward service/zipkin 9411:9411
```

```
minikube start

kubectl get node
minikube addons list
minikube addons enable dashboard
minikube addons enable ingress               -> reverse proxy
minikube addons enable ingress-dns           -> service discovery
minikube addons enable metrics-server        -> application metrics
minikube dashboard
```

"provision_key": "CGeEYOzekYDvxP1u7tbm3XgOaSbEewls",

"client_id": "wvPfJIZR3tF5AB0U6duXcsUIfa97GTqf",
"client_secret": "LRmQ6hfqcDvQl69W4DjdsGQc1FUmoewG"

"secret": "cRtFl18ph94P1obX3EIqMGaOwOqfjpOj",
"key": "UHqW0uLqkxawd7iCkM8ocEPO2tkpBKsd",

admin : "q29dJ7pmw19cYV2gc0AkG2CtqdUbnnSO"

imc: "0KxJrxjyxaKQnXUkpcmlBAPvcyBcT1wo"

```
http://localhost:8000/api/news
```

```
docker system prune --all
docker volume prune

https://minikube.sigs.k8s.io/docs/start/
```

```
http://github.com/sommaik/devops

kubectl create secret docker-registry regcred
--docker-server=registry.gitlab.com
--docker-username=sommai.k@gmail.com
--docker-password=xxxxxxxxxxxxxxxx
--docker-email=sommai.k@gmail.com
```

Oauth 2 kong

```
"provision_key": "eaztNq8KRqu95zn6mWS76k4iFJiNgW7l",
"client_id": "Uk3Cz6XIUJp1L5P2tY0X954T2itIOGD3",
"client_secret": "yIzfrLm60fqXaPOQdoZr040VFBb0yokl"
"name": "bob",
```

JWT Kong

```
"iss": "ZsQ9Iigp44GLe0T5wPtgFDLX1SwFq6NE",
"secret": "3VJafNhIRUilKZ4HaFNLSp4tqi96oovV",
```

User Key

```
super = XVIYl5tI5J03WylSdcPtBst1C0KwF2E4
imc = 	UL28mTj5dFtgezEHpLBjc0e8QSGr7sZ9
```

## Custom kong plugins

```
git clone https://github.com/Kong/kong-pongo.git

git clone https://github.com/Kong/kong-plugin.git

../kong-pongo/pongo.sh run

../kong-pongo/pongo.sh shell

kong migrations bootstrap --force

kong start

curl -i -X POST \
 --url http://localhost:8001/services/ \
 --data 'name=example-service' \
 --data 'url=http://konghq.com'

curl -i -X POST \
 --url http://localhost:8001/services/example-service/routes \
 --data 'hosts[]=example.com'

curl -i -X POST \
--url http://localhost:8001/services/example-service/plugins/ \
--data 'name=api-version'

curl -I -H "Host: example.com" http://localhost:8000/

```
 ## oauth2
```
"provision_key": "REujl7mls3JWCSvkDDqVSqWrKiAxuWVk",

"name": "auser",
"client_secret": "wQ7XKsqwhkfsPyPLhLS8dWlOVX11qnPX",
"client_id": "lqOANluuRBzELRyKTXup3CJHeWRfyLJx",
```

## JWT
```
"secret": "D6LcjYEuZAjEgdqSz3OAEUgRT1F2vl6q",
"key": "NxYOZbiNVlsHJ1N26ZQQRclIzohEecbM",
```

## ACL
```
usr = gNUh74YKwObYZg4199a7eWEVhNqIHrQX G002
imc = pGsJ7b6otq3chIt5RhJNn46tQtxkISR5 G001
```

```
kubectl port-forward deployment/kong 8002:8002 --address=0.0.0.0

kubectl port-forward deployment/kong 8001:8001 --address=0.0.0.0

```